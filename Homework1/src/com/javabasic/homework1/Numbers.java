package com.javabasic.homework1;

import java.sql.SQLOutput;
import java.util.Random;
import java.util.Scanner;

public class Numbers {

    public static void main(String[] args) {

        FindNumber();
    }

    private static void FindNumber(){
        Random randomNumber = new Random();
        int number = randomNumber.nextInt(101);

        Scanner in = new Scanner(System.in);
        System.out.println("Hello! What's your name?");
        String name = in.nextLine();

        System.out.println("Let the game begin!");

        int userNumber;

        while (true) {
            System.out.println("Please, guess the number from 0 to 100");

            userNumber = in.nextInt();

            if(userNumber == number){
                break;
            }
            if (userNumber < number) {
                System.out.println("Your number is too small. Please try again.");
            }
            if (userNumber > number) {
                System.out.println("Your number is too big. Pleas, try again.");
            }

        }
        in.close();
        System.out.println("Congratulations, " + name + "!");

    }

}
