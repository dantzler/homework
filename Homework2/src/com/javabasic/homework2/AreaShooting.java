package com.javabasic.homework2;

import java.util.Random;
import java.util.Scanner;

public class AreaShooting {

    public static void main(String[] args) {
        AreaShootingGame();
    }

    private static void AreaShootingGame() {

        //field with a random cell with a target

        Object[][] field = {
                {'0', '1', '2', '3', '4', '5'},
                {'1', '-', '-', '-', '-', '-'},
                {'2', '-', '-', '-', '-', '-'},
                {'3', '-', '-', '-', '-', '-'},
                {'4', '-', '-', '-', '-', '-'},
                {'5', '-', '-', '-', '-', '-'}
        };


        Random random = new Random();
        int targetRow = random.nextInt(5) + 1;
        int targetColumn = random.nextInt(5) + 1;
        field[targetRow][targetColumn] = '-';

        System.out.println("All set. Get ready to rumble!");

        Scanner in = new Scanner(System.in);
        int shootingRow = 0;
        int shootingColumn = 0;


        //player input

        while (true) {

            String inputRow;
            String inputColumn;
            boolean rowIsNumber = false;
            boolean columnIsNumber = false;

            while (!rowIsNumber) {

                try {
                    System.out.println("Please, enter a number for the field horizontal line.");
                    inputRow = in.nextLine();
                    shootingRow = Integer.parseInt(inputRow);
                    if (shootingRow > 0 && shootingRow < 6) {
                        rowIsNumber = true;
                    } else {
                        System.out.println("Your input is incorrect. Please, enter numbers from 1 to 5");
                    }
                } catch (NumberFormatException e) {
                    System.out.println("Your input is not a valid number, please try again");
                }
            }

            while (!columnIsNumber) {

                try {
                    System.out.println("Please, enter a number for the field vertical line.");
                    inputColumn = in.nextLine();
                    shootingColumn = Integer.parseInt(inputColumn);

                    if (shootingColumn > 0 && shootingColumn < 6) {
                        columnIsNumber = true;
                    } else {
                        System.out.println("Your input is incorrect. Please, enter numbers from 1 to 5");
                    }

                } catch (NumberFormatException e) {
                    System.out.println("Your input is not a valid number, please try again");
                }
            }


            if (shootingRow == targetRow && shootingColumn == targetColumn) {
                break;
            } else {
                field[shootingRow][shootingColumn] = '*';

                for (int i = 0; i < field.length; i++) {
                    for (int j = 0; j < field[i].length; j++) {
                        System.out.print(field[i][j] + " | ");
                    }
                    System.out.println();
                }
            }
        }

        // game end with the final field showing player inputs and the target cell

        System.out.println("You have won!");
        field[targetRow][targetColumn] = 'x';

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                System.out.print(field[i][j] + " | ");
            }
            System.out.println();
        }

    }
}








