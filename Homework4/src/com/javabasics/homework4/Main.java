package com.javabasics.homework4;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        Human uncle = new Human();
        Human childMother = new Human("Jane", "Karleone");
        Human childFather = new Human("Vito", "Karleone");
        Pet childDog = new Pet("dog", "Rock", 5, 75, new String[]{"eat", "drink", "sleep"});
        Human child = new Human("Michael", "Karleone", 1977, childMother, childFather, childDog, 90, new String[][]{
                {"monday", "trip to Chicago"},
                {"tuesday", "meet Roberto Triviani"},
                {"wednesday", "buy restaurant in Hyde Park"},
                {"thursday", "return home"},
                {"friday", "soccer game with irish"},
                {"saturday", "fly to Napoli"},
                {"sunday", "climb Vesuvius"}
        });

        System.out.println(child.toString());
        child.describePet(childDog);
        child.greetPet();

        System.out.println(childDog.toString());
        childDog.eat();
        childDog.respond();
        childDog.foul();

        uncle.setName("Franco");
        uncle.setSurname("Tozzi");
        uncle.setYear(1950);
        uncle.setIq(99);
        uncle.setSchedule(new String[][]{
                {"mon", "play video games"},
                {"tue", "eat a cake"},
                {"wed", "jump with a parachute"},
                {"thu", "meet Vito"},
                {"fr", "guitar lesson"},
                {"sat", "buy a monkey"},
                {"sun", "play with the monkey"},
        });

        Pet unclePet = new Pet();
        unclePet.setSpecies("monkey");
        unclePet.setNickname("Mafio-the-lazy-cat");
        unclePet.setTrickLevel(30);
        unclePet.setHabits(new String[]{"eat", "sleep", "chase mice"});
        uncle.describePet(unclePet);


        System.out.println(uncle.getName());
        System.out.println(uncle.getSurname());
        System.out.println(uncle.getYear());
        System.out.println(uncle.getIq());
        System.out.println(Arrays.deepToString(uncle.getSchedule()));
        uncle.describePet(unclePet);
    }
}