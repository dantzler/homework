package com.javabasics.homework4;

import java.util.Arrays;

public class Pet {

    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits = {};

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public String getNickname(){
        return this.nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getSpecies() {
        return this.species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this(species, nickname);
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public int getAge(){
        return this.age;
    }
    public void setAge(int age){
        this.age = age;
    }

    public int getTrickLevel(){
        return this.trickLevel;
    }

    public void setTrickLevel(int trickLevel){
        if(trickLevel >= 0 && trickLevel <= 100){
            this.trickLevel = trickLevel;
        }
    }

    public String [] getHabits(){
        return this.habits;
    }
    public void setHabits(String [] habits){
        this.habits = habits;
    }

    Pet() {

    }


    void eat() {

        System.out.println("I'm eating!");
    }

    void respond() {

        System.out.printf("Hello, Master. I am %s. I missed you! \n", getNickname());
    }

    void foul() {

        System.out.println("I've got to cover my tracks well...");
    }

    @Override
    public String toString(){
        return getSpecies() + "{nickname="+getNickname() + ", age="+getAge() + ", trickLevel=" + getTrickLevel()
                + ", habits=" + Arrays.toString(getHabits()) + "}";
    }
}



