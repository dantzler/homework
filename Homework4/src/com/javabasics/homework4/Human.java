package com.javabasics.homework4;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Human mother;
    private Human father;
    private String [][] schedule = new String[7][2];

    void greetPet(){
        System.out.println("Hello, " + pet.getNickname());
    }

    public String petTrickiness(int trickiness){
        if(trickiness <= 50){
            return "almost not tricky";
        }
            return "very tricky";
    }

    void describePet(Pet pet){
        System.out.printf("I have a %s. It's %d years old. It's %s \n", pet.getSpecies(), pet.getAge(), petTrickiness(pet.getTrickLevel()));
    }

    Human () {};

    Human (String name, String surname){
        this.name = name;
        this.surname = surname;
    }

    Human (String name, String surname, int year){
        this(name, surname);
        this.year = year;
    }

    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getSurname(){
        return this.surname;
    }
    public void setSurname(String surname){
        this.surname = surname;
    }

    public int getYear(){
        return this.year;
    }
    public void setYear(int year){
        this.year = year;
    }

    Human (String name, String surname, int year, Human mother, Human father){
        this(name, surname, year);
        this.mother = mother;
        this.father = father;
    }


    Human (String name, String surname, int year, Human mother, Human father, Pet pet, int iq, String [][] schedule){
        this(name, surname, year, mother, father);
        this.pet = pet;
        this.schedule = schedule;
        this.iq = iq;
    }

    public String [][] getSchedule(){
        return this.schedule;
    }
    public void setSchedule(String [][] schedule){
        this.schedule = schedule;
    }

    public int getIq(){
        return this.iq;
    }
    public void setIq(int iq){
        if(iq >= 0 && iq <= 100){
            this.iq = iq;
        }
    }

    @Override
    public String toString() {
        return "Human{name=" + getName() + ", surname=" + getSurname() + ", year=" + getYear() + ", iq=" + getIq()
                + ", mother = " + this.mother.name + " "+ this.mother.surname + ", father=" + this.father.name + " "
                + this.father.surname + ", pet=" + this.pet + "}";
    }
}


