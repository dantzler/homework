package com.javabasics.homework3;

import java.util.Scanner;

public class WeeklyPlanner {

    public static void main(String[] args) {

        String[][] schedule = {
                {"Sunday", "clean room"},
                {"Monday", "go to Java class"},
                {"Tuesday", "go to BA class"},
                {"Wednesday", "dance class"},
                {"Thursday", "BA class"},
                {"Friday", "BA meet up"},
                {"Saturday", "go for a run"}
        };

        Scanner in = new Scanner(System.in);

        while (true) {
            System.out.println("Please, input the day of the week.");
            String dayOfWeek = in.nextLine();

            switch (dayOfWeek.trim().toLowerCase()) {
                case "change":

                case "sunday":
                    System.out.println("Your tasks for Sunday: " + schedule[0][1]);
                    break;
                case "monday":
                    System.out.println("Your tasks for Monday: " + schedule[1][1]);
                    break;
                case "tuesday":
                    System.out.println("Your tasks for Tuesday: " + schedule[2][1]);
                    break;
                case "wednesday":
                    System.out.println("Your tasks for Wednesday: " + schedule[3][1]);
                    break;
                case "thursday":
                    System.out.println("Your tasks for Thursday: " + schedule[4][1]);
                    break;
                case "friday":
                    System.out.println("Your tasks for Friday: " + schedule[5][1]);
                    break;
                case "saturday":
                    System.out.println("Your tasks for Saturday: " + schedule[6][1]);
                    break;
                case "exit":
                    return;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }
}

